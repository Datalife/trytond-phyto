# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.

from trytond.model import ModelSQL, ModelView, ModelSingleton
from trytond.modules.company.model import CompanyMultiValueMixin

__all__ = ['Configuration']


class Configuration(ModelSingleton, ModelSQL, ModelView,
        CompanyMultiValueMixin):
    """Phytosanitary Configuration"""
    __name__ = 'phyto.configuration'
