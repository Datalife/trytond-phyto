# this repository contains the full copyright notices and license terms.
from proteus import Model

__all__ = ['create_applier']


def _get_category(type_='Cualified'):
    PartyCategory = Model.get('party.category')
    category, = PartyCategory.find([
        ('parent.name', '=', 'Applier'),
        ('name', '=', type_)])

    return category


def create_applier(config=None, name='Applier 1', type_='Cualified',
        phyto_party=None):

    Party = Model.get('party.party')
    applier = Party(name=name)
    applier.categories.append(_get_category(type_))
    applier.phyto_party.append(phyto_party)
    applier.save()

    return applier
